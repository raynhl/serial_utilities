# Serial Port Utility

This is a package container serial port utilities. As of now, it contains has a virtual serial port splitter with the ability to do serial input recording.
It also contains a unit tester that would be able to automate serial input based on a user-provided test file.



## Getting Started


### Installing dependency

Firstly, go to config\pair_info.config and define your virtual serial port pair names. 
As a rule of thumb, use a higher COM# to avoid conflict with the system's existing COM ports.
For example:


```
COM21:COM22
COM23:COM24
COM25:COM26
```


Then, from the main directory, run the following script to configure and install the required dependencies, namely com0com and progressbar-lib.
***You will need to Run As Administator***


```
python setup.py
```



## Usage HOW-TO


### Virtual Serial Port Splitter



The virtual serial port splitter script is in serial_port_splitter.py.
Basically, the script will split a physical serial port into multiple virtual ports that was defined earlier in config\pair_info.config.

There are several command line arguments that can be passed into this script. The following are the arguments definitions:

_________________________________________________________________________________________
	-h		| Help
			|
	-b		| Physical serial port. Example: COM10
			|
	-n		| Number of virtual serial port to split into. 
			|
	-v		| Virtual serial port(s). If multiple entries, separate ports with commas.
			| [eg: COM21,COM23,COM25] 
			|
	-br		| Baud rate for all serial ports. Optional argument. Default is set to 115200
			|
	-r		| Filename of serial input through virtual serial ports. 
			| Recommended location to save this file is in unit_tests\ directory
			| If this option is not defined, recording will be disabled.
_________________________________________________________________________________________ 


As an example. Let's say we want to split our physical serial port, COM10 into 3 virtual serial ports (COM21, COM23, COM25) and enable recording to the file my_serial_recording.test in unit_tests\ directory. All serial ports have baud rate 115200


```
python serial_port_splitter.py -b COM10 -n 3 -v COM21,COM23,COM25 -r unit_tests\my_serial_recording.test
```


### Unit Tester



The unit tester is a script that will automate the entry into BIOS Setup Menu and perform automated changing
of options as recorded by user.

Again, the script has several command line arguments. The following are the arguments definitions:

_________________________________________________________________________________________
	-h		| Help
			|
	-v		| Virtual serial port. Example: COM21
			|
	-br		| Baud rate for virtual port. Optional argument. Default is set to 115200 
			|
	-f		| Input file for the recorded sequences after entering BIOS Setup Menu.
			|
	-t		| Delay between steps during automated input. Default is 1 second.
			|
	-s		| User-defined trigger string to start attempts to enter BIOS Setup Menu
			|
	-p		| Postcode string to indicate BIOS Setup Menu
_________________________________________________________________________________________ 


As an example, we have now split our physical serial port into three virtual serial ports (COM21, COM23, COM25) and we want to perform automated unit_test on COM23. The recorded unit test file is located at unit_tests\my_serial_recording.test. We want the stepping between automated entry to be 2 seconds. The trigger string is "Showing progress bar" and the post code string to show we are at BIOS Setup Menu is "<AC"

```
python unit_tester.py -v COM23 -f unit_tests\my_serial_recording.test -t 2 -s "Showing progress bar" -p "<AC>"
```

## Acknowledgement

This package leverages some other works done by other authors.
All libraries and applications belongs to their respective owners.


## Disclaimer

These utilities are created to help facilitate testing in my daily job as a BIOS Engineer.

IF you think you can use this somewhere in your workflow, do feel free to fork and use this utilities.

For any questions or compliments, please reach me at <nhlhanlim93@outlook.com> or visit my webpage <https://www.rayng.xyz>

